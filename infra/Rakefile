# frozen_string_literal: true

require 'rubocop/rake_task'
require 'yamllint/rake_task'
require 'cfndsl/rake_task'
require 'aws-sdk-apigateway'
require 'aws-sdk-cloudformation'
require 'aws-sdk-iam'
require 'aws-sdk-s3'
require 'aws-sdk-sts'
require 'aws-sdk-lambda'
require 'aws-sdk-kms'
require 'yaml'
require 'json'
require 'securerandom'

# Load application config file
CONFIG = YAML.load_file('config/app.yml').freeze

# Import resource tags from config
TAGS = [] # rubocop:disable Style/MutableConstant
CONFIG['tags'].each_pair do |k, v|
    next if k == 'TIER'

    TAGS << { key: k, value: v.to_s }
end
TAGS << { key: 'TIER', value: 'Production' }
TAGS.freeze

# Configure stack states
VALID_STACK_STATES = %w[
    CREATE_COMPLETE
    ROLLBACK_COMPLETE
    UPDATE_ROLLBACK_COMPLETE
    UPDATE_COMPLETE
].freeze

VALID_STACK_SET_STATES = %w[
    ACTIVE
].freeze

# Rubocop task
RuboCop::RakeTask.new

# Yamllint task
YamlLint::RakeTask.new do |t|
    t.paths = %w[
        config/app.yml
        .rubocop.yml
    ]
end

# Test task, runs rubocop & yamllint
task test: %i[rubocop yamllint]

# Task to check if a deployment environment variable has been set
task :check_deploy_env do
    unless ENV['DEPLOYENV']
        warn 'please provide a DEPLOYENV'
        exit 1
    end
end

# Stack deployment, generates and executes stack changes set
# rubocop:disable Metrics/MethodLength, Metrics/AbcSize
# rubocop:disable Metrics/PerceivedComplexity, Metrics/CyclomaticComplexity
def deploy_stack(stackname, parameters = [], env = '')
    p parameters
    stack_exists = false
    # Read the generated stack CF template
    template = File.read("output/#{stackname}.json")
    # Set stack name, include env if passed in
    # ensures unique stack sets for multi account deployments such as pipeline roles
    stack_name = if ['', 'edge'].include?(env)
                     "#{CONFIG['prefix']}-#{stackname}".tr('_', '-')
                 else
                     "#{CONFIG['prefix']}-#{stackname}-#{env}".tr('_', '-')
                 end
    change_set_name = "#{stack_name}-#{SecureRandom.uuid}"
    # Populate stack attributes
    stack_data = {
        stack_name: stack_name,
        template_body: template,
        capabilities: %w[CAPABILITY_NAMED_IAM CAPABILITY_AUTO_EXPAND],
        parameters: parameters,
        tags: TAGS,
        change_set_name: change_set_name
    }
    cfn = if env == 'edge'
              Aws::CloudFormation::Client.new(
                  region: 'us-east-1'
              )
          else
              Aws::CloudFormation::Client.new
          end
    # List all valid stacks
    stacks = cfn.list_stacks(
        stack_status_filter: VALID_STACK_STATES
    )
    # Look for existing stack
    stacks[:stack_summaries].each do |stack|
        next unless stack[:stack_name] == stack_name
        next unless VALID_STACK_STATES.include? stack[:stack_status]

        stack_exists = true
    end
    if stack_exists
        # Stack already exists
        action = 'update'
    else
        # No existing stack
        stack_data['change_set_type'] = 'CREATE'
        action = 'build'
    end

    # Generate & exucute change set
    begin
        puts "Creating change set: #{change_set_name}"
        cfn.create_change_set stack_data
        sleep 5
        puts 'Waiting for change set to complete...'
        begin
            cfn.wait_until(:change_set_create_complete, change_set_name: change_set_name, stack_name: stack_name)
            puts 'Executing change set.'
            cfn.execute_change_set(change_set_name: change_set_name, stack_name: stack_name)
            sleep 5
            puts "Waiting for stack to #{action}..."
            begin
                if stack_exists
                    cfn.wait_until(:stack_update_complete, stack_name: stack_name)
                else
                    cfn.wait_until(:stack_create_complete, stack_name: stack_name)
                end
                puts "#{stack_name} stack #{action} completed!"
            rescue Aws::Waiters::Errors::WaiterFailed => e
                p e
                exit 1
            end
        rescue Aws::Waiters::Errors::WaiterFailed => e
            p e
            error = cfn.describe_change_set(change_set_name: change_set_name, stack_name: stack_name)
            p error.status_reason
        end
    rescue Aws::CloudFormation::Errors::ServiceError => e
        p e
        exit 1
    end
end
# rubocop:enable Metrics/MethodLength, Metrics/AbcSize
# rubocop:enable Metrics/PerceivedComplexity, Metrics/CyclomaticComplexity

# Generates Cloudformation template for pipeline roles
CfnDsl::RakeTask.new(:generate_pipeline_roles) do |t|
    case ENV['AWS_PROFILE']
    when CONFIG['awsprofile_dev']
        env = 'dev'
    when CONFIG['awsprofile_test']
        env = 'test'
    when CONFIG['awsprofile_prod']
        env = 'prod'
    end
    CfnDsl.disable_binding
    CfnDsl::ExternalParameters.defaults(deployenv: env)
    t.cfndsl_opts = {
        verbose: true,
        pretty: false,
        files: [{
            filename: 'pipeline/roles.rb',
            output: 'output/pipeline_roles.json'
        }],
        extras: [
            [:yaml, 'config/app.yml']
        ]
    }
end

task roles: %i[test generate_pipeline_roles] do
    case ENV['AWS_PROFILE']
    when CONFIG['awsprofile_dev']
        env = 'dev'
    when CONFIG['awsprofile_test']
        env = 'test'
    when CONFIG['awsprofile_prod']
        env = 'prod'
    end
    role_params = [
        {
            parameter_key: 'prefix',
            parameter_value: CONFIG['prefix'].to_s
        },
        {
            parameter_key: 'deployenv',
            parameter_value: env
        },
        {
            parameter_key: 'reprojectimage',
            parameter_value: CONFIG['reprojectimage'].to_s
        }
    ]
    puts "Deploying roles for CodePipeline deployments in #{env} account"
    deploy_stack('pipeline_roles', role_params, env)
end

# Generates Cloudformation template for pipeline resources bucket
CfnDsl::RakeTask.new(:generate_resource_bucket) do |t|
    CfnDsl.disable_binding
    t.cfndsl_opts = {
        verbose: true,
        pretty: false,
        files: [{
            filename: 'pipeline/resources.rb',
            output: 'output/pipeline_resources.json'
        }],
        extras: [
            [:yaml, 'config/app.yml']
        ]
    }
end

# Generates Cloudformation template for CI/CD pipeline
CfnDsl::RakeTask.new(:generate_pipeline) do |t|
    CfnDsl.disable_binding
    t.cfndsl_opts = {
        verbose: true,
        pretty: false,
        files: [{
            filename: 'pipeline/pipeline.rb',
            output: 'output/pipeline.json'
        }],
        extras: [
            [:yaml, 'config/app.yml']
        ]
    }
end

task pipeline: %i[test generate_pipeline_roles generate_resource_bucket generate_pipeline] do
    # ------ Pipeline Resources Stack ------ #
    puts 'Deploying pipeline resource stack'
    resource_params = [
        {
            parameter_key: 'prefix',
            parameter_value: CONFIG['prefix'].to_s
        }
    ]
    deploy_stack('pipeline_resources', resource_params)
    system('./bin/resources_npminstall.sh')
    slack_path = { path: CONFIG['slackpath'].to_s }
    File.open('pipeline/resources/slack_notification/config.json', 'w') do |file|
        file.write JSON.generate(slack_path)
    end
    system('cd pipeline/resources/slack_notification; zip -r9 ../../../output/slack_notification.zip .')
    slacknotify_sha256 = Digest::SHA256.file 'output/slack_notification.zip'
    s3 = Aws::S3::Resource.new
    slacknotify_obj = s3.bucket("#{CONFIG['prefix']}-pipeline-resources").object(slacknotify_sha256.hexdigest)
    puts 'Uploading pipeline resource functions to resource bucket'
    slacknotify_obj.upload_file('output/slack_notification.zip')

    # ------ Pipeline Stack ------ #
    pipeline_params = [
        {
            parameter_key: 'prefix',
            parameter_value: CONFIG['prefix'].to_s
        },
        {
            parameter_key: 'slacknotifykey',
            parameter_value: slacknotify_sha256.hexdigest
        },
        {
            parameter_key: 'ecruri',
            parameter_value: CONFIG['ecruri'].to_s
        }
    ]
    puts 'Deploying pipeline stack'
    deploy_stack('pipeline', pipeline_params)
end

# Generates ECR instance
CfnDsl::RakeTask.new(:generate_ecr) do |t|
    CfnDsl.disable_binding
    t.cfndsl_opts = {
        verbose: true,
        pretty: true,
        files: [{
            filename: 'pipeline/ecr.rb',
            output: 'output/ecr.json'
        }],
        extras: [
            [:yaml, 'config/app.yml']
        ]
    }
end

task ecr: %i[test generate_ecr] do
    deploy_stack('ecr')
    # Add deployed ECR instance URI to app config
    sts = Aws::STS::Client.new
    id = sts.get_caller_identity
    new_config = CONFIG.dup
    new_config['ecruri'] = [id.account, 'dkr.ecr', CONFIG['region'].to_s, "amazonaws.com/#{CONFIG['prefix']}"].join('.')
    File.open('config/app.yml', 'w') do |file|
        file.write new_config.to_yaml
    end
end

task fetchapi: %i[check_deploy_env] do
    apigateway = Aws::APIGateway::Client.new
    api_config = {}
    # Fetch API ID
    apigateway.get_rest_apis.each do |response|
        response.items.each do |item|
            regex = Regexp.new([CONFIG['prefix'], ENV['DEPLOYENV']].join('-'))
            # Check if api name matches API for this deployenv
            next unless regex.match(item.name)

            # Record api id in config
            api_config['apiid'] = item.id
        end
    end
    # Write config file
    File.open('distribution/config/apigateway.yml', 'w') do |file|
        file.write api_config.to_yaml
    end
end
