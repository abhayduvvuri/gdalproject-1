# frozen_string_literal: true

require 'cfndsl'

CloudFormation do
    params = {}
    external_parameters.each_pair do |key, val|
        key = key.to_sym
        params[key] = val
    end
    Description 'A pipeline to transpile CFNDSL and deploy the cloudformation output'
    Transform 'AWS::Serverless-2016-10-31'

    #
    ##
    ### Parameters
    ##
    #
    Parameter(:prefix) do
        Type String
        AllowedPattern '[a-z]*[-a-z0-9]*'
        ConstraintDescription 'A lower case string between 3 and 16 characters'
        Description 'A lower case string between 3 and 16 characters'
        MinLength 3
        MaxLength 16
    end

    Parameter(:slacknotifykey) do
        Type String
        AllowedPattern '[a-z0-9]*[-a-z0-9]*'
    end

    Parameter(:ecruri) do
        Type String
        AllowedPattern '[0-9]*.dkr.ecr.[a-z]{2}-[a-z]*-[0-9].amazonaws.com/[a-z]*'
    end

    #
    ##
    ### IAM - Group, User, Roles & Policies, KMS Key & Alias
    ##
    #
    # IAM Policy - Bitbucket
    IAM_Policy(:bitbucketartifactspolicy) do
        PolicyName FnJoin('', [Ref(:prefix), '-bitbucketartifacts'])
        Groups [FnJoin('', [Ref(:prefix), '-bitbucket'])]
        PolicyDocument(
            Version: '2012-10-17',
            Statement: [
                {
                    Effect: 'Allow',
                    Resource: [
                        FnGetAtt(:artifactsbucket, 'Arn'),
                        FnJoin('', [FnGetAtt(:artifactsbucket, 'Arn'), '/*'])
                    ],
                    Action: [
                        's3:PutObjectVersionTagging',
                        's3:ListMultipartUploadParts',
                        's3:PutObject',
                        's3:AbortMultipartUpload',
                        's3:PutObjectVersionAcl',
                        's3:PutObjectTagging',
                        's3:PutObjectAcl'
                    ]
                }
            ]
        )
    end
    # KMS Key
    KMS_Key(:kmskey) do
        EnableKeyRotation TRUE
        KeyPolicy(
            Version: '2012-10-17',
            Id: Ref(:prefix),
            Statement: [
                {
                    Sid: 'Allows admin of the key',
                    Effect: 'Allow',
                    Principal: {
                        AWS: FnSub('arn:aws:iam::${AWS::AccountId}:root')
                    },
                    Action: [
                        'kms:Create*',
                        'kms:Describe*',
                        'kms:Enable*',
                        'kms:List*',
                        'kms:Put*',
                        'kms:Update*',
                        'kms:Revoke*',
                        'kms:Disable*',
                        'kms:Get*',
                        'kms:Delete*',
                        'kms:ScheduleKeyDeletion',
                        'kms:CancelKeyDeletion'
                    ],
                    Resource: '*'
                }, {
                    Sid: 'Allow use of the key',
                    Effect: 'Allow',
                    Principal: {
                        AWS: [
                            "arn:aws:iam::#{params[:accountids]['development']}:root",
                            "arn:aws:iam::#{params[:accountids]['testing']}:root",
                            "arn:aws:iam::#{params[:accountids]['production']}:root",
                            FnJoin('', ["arn:aws:iam::#{params[:accountids]['development']}:role/", Ref(:prefix), '-codebuild']),
                            FnJoin('', ["arn:aws:iam::#{params[:accountids]['development']}:role/", Ref(:prefix), '-codepipeline']),
                            FnJoin('', ["arn:aws:iam::#{params[:accountids]['development']}:role/", Ref(:prefix), '-s3deploy']),
                            FnJoin('', ["arn:aws:iam::#{params[:accountids]['testing']}:role/", Ref(:prefix), '-s3deploy']),
                            FnJoin('', ["arn:aws:iam::#{params[:accountids]['production']}:role/", Ref(:prefix), '-s3deploy'])
                        ]
                    },
                    Action: [
                        'kms:Encrypt',
                        'kms:Decrypt',
                        'kms:ReEncrypt*',
                        'kms:GenerateDataKey*',
                        'kms:DescribeKey'
                    ],
                    Resource: '*'
                }
            ]
        )
        # Add tags. Skip TIER tag, always tagged as Production
        params[:tags].each_pair do |k, v|
            next if k == 'TIER'

            add_tag(k, v)
        end
        add_tag('TIER', 'Production')
    end

    # KMS Alias
    KMS_Alias(:kmsalias) do
        AliasName FnJoin('/', ['alias', Ref(:prefix)])
        TargetKeyId Ref(:kmskey)
    end

    # IAM Role - Lambda execution role for Slack Notify Lambda
    IAM_Role(:lambdaexecutionrole) do
        RoleName FnJoin('-', [Ref(:prefix), 'lambda-slack-notification'])
        AssumeRolePolicyDocument(
            Version: '2012-10-17',
            Statement: [
                {
                    Action: ['sts:AssumeRole'],
                    Effect: 'Allow',
                    Principal: {
                        Service: 'lambda.amazonaws.com'
                    }
                }
            ]
        )
        ManagedPolicyArns(
            [
                'arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole'
            ]
        )
    end

    # IAM Policy (codebuild)
    IAM_Policy(:codebuildpolicy) do
        PolicyName FnJoin('', [Ref(:prefix), '-codebuild'])
        PolicyDocument(
            Version: '2012-10-17',
            Statement: [
                {
                    Effect: 'Allow',
                    Resource: '*',
                    Action: [
                        'logs:CreateLogGroup',
                        'logs:CreateLogStream',
                        'logs:PutLogEvents'
                    ]
                }, {
                    Effect: 'Allow',
                    Resource: '*',
                    Action: [
                        's3:ListObjects',
                        's3:ListAllMyBuckets',
                        's3:HeadBucket'
                    ]
                }, {
                    Effect: 'Allow',
                    Action: [
                        'cloudformation:*'
                    ],
                    Resource: '*'
                }
            ]
        )
        Roles [FnJoin('', [Ref(:prefix), '-codebuild'])]
    end

    # IAM Policy (codepipeline)
    IAM_Policy(:codepipelinepolicy) do
        DependsOn %i[artifactsbucketpolicy]
        PolicyName FnJoin('', [Ref(:prefix), '-codepipeline'])
        PolicyDocument(
            Version: '2012-10-17',
            Statement: [
                {
                    Action: [
                        's3:PutObject'
                    ],
                    Resource: [
                        'arn:aws:s3:::codepipeline*'
                    ],
                    Effect: 'Allow'
                },
                {
                    Action: [
                        's3:*',
                        'sns:*'
                    ],
                    Resource: '*',
                    Effect: 'Allow'
                },
                {
                    Action: [
                        'codebuild:BatchGetBuilds',
                        'codebuild:StartBuild'
                    ],
                    Resource: '*',
                    Effect: 'Allow'
                },
                {
                    Action: [
                        'sts:AssumeRole'
                    ],
                    Resource: [
                        FnJoin('', ["arn:aws:iam::#{params[:accountids]['development']}:role/", Ref(:prefix), '-codepipelinecloudformationrole']),
                        FnJoin('', ["arn:aws:iam::#{params[:accountids]['testing']}:role/", Ref(:prefix), '-codepipelinecloudformationrole']),
                        FnJoin('', ["arn:aws:iam::#{params[:accountids]['production']}:role/", Ref(:prefix), '-codepipelinecloudformationrole'])
                    ],
                    Effect: 'Allow'
                }
            ]
        )
        Roles [FnJoin('', [Ref(:prefix), '-codepipeline'])]
    end

    # IAM Policy (cloudwatch event)
    IAM_Policy(:cloudwatcheventpolicy) do
        PolicyName FnJoin('', [Ref(:prefix), '-cloudwatchevent'])
        PolicyDocument(
            Version: '2012-10-17',
            Statement: [
                {
                    Effect: 'Allow',
                    Resource: [
                        FnJoin('', ['arn:aws:codepipeline:', Ref('AWS::Region'), ':', Ref('AWS::AccountId'), ':', Ref(:prefix)])
                    ],
                    Action: [
                        'codepipeline:StartPipelineExecution'
                    ]
                }
            ]
        )
        Roles [Ref(:cloudwatcheventrole)]
    end

    # IAM Role (cloudwatch event)
    IAM_Role(:cloudwatcheventrole) do
        RoleName FnJoin('', [Ref(:prefix), '-cloudwatcheventrole'])
        AssumeRolePolicyDocument(
            Version: '2012-10-17',
            Statement: [
                {
                    Action: [
                        'sts:AssumeRole'
                    ],
                    Effect: 'Allow',
                    Principal: {
                        Service: 'events.amazonaws.com'
                    }
                }
            ]
        )
    end

    #
    ##
    ### S3 - Bucket & events
    ##
    #

    # S3 Bucket - Pipeline Artifacts
    S3_Bucket(:artifactsbucket) do
        BucketName FnJoin('', [Ref(:prefix), '-artifacts'])
        VersioningConfiguration(
            Status: 'Enabled'
        )
        # Add tags. Skip TIER tag, always tagged as Production
        params[:tags].each_pair do |k, v|
            next if k == 'TIER'

            add_tag(k, v)
        end
        add_tag('TIER', 'Production')
    end

    # S3 Bucket Policy - CodeBuild & CodePipeline
    S3_BucketPolicy(:artifactsbucketpolicy) do
        Bucket Ref(:artifactsbucket)
        PolicyDocument(
            Statement: [
                {
                    Sid: 'AccessForBuildandDeploy',
                    Action: [
                        - 's3:*'
                    ],
                    Effect: 'Allow',
                    Resource: [
                        FnGetAtt(:artifactsbucket, 'Arn'),
                        FnJoin('', [FnGetAtt(:artifactsbucket, 'Arn'), '/*'])
                    ],
                    Principal: {
                        AWS: [
                            FnJoin('', ["arn:aws:iam::#{params[:accountids]['development']}:role/", Ref(:prefix), '-codepipelinecloudformationrole']),
                            FnJoin('', ["arn:aws:iam::#{params[:accountids]['development']}:role/", Ref(:prefix), '-cloudformationdeployrole']),
                            FnJoin('', ["arn:aws:iam::#{params[:accountids]['development']}:role/", Ref(:prefix), '-codebuild']),
                            FnJoin('', ["arn:aws:iam::#{params[:accountids]['development']}:role/", Ref(:prefix), '-codepipeline']),
                            FnJoin('', ["arn:aws:iam::#{params[:accountids]['development']}:role/", Ref(:prefix), '-s3deploy']),
                            FnJoin('', ["arn:aws:iam::#{params[:accountids]['testing']}:role/", Ref(:prefix), '-codepipelinecloudformationrole']),
                            FnJoin('', ["arn:aws:iam::#{params[:accountids]['testing']}:role/", Ref(:prefix), '-cloudformationdeployrole']),
                            FnJoin('', ["arn:aws:iam::#{params[:accountids]['testing']}:role/", Ref(:prefix), '-lambdaexecution']),
                            FnJoin('', ["arn:aws:iam::#{params[:accountids]['testing']}:role/", Ref(:prefix), '-s3deploy']),
                            FnJoin('', ["arn:aws:iam::#{params[:accountids]['production']}:role/", Ref(:prefix), '-codepipelinecloudformationrole']),
                            FnJoin('', ["arn:aws:iam::#{params[:accountids]['production']}:role/", Ref(:prefix), '-cloudformationdeployrole']),
                            FnJoin('', ["arn:aws:iam::#{params[:accountids]['production']}:role/", Ref(:prefix), '-lambdaexecution']),
                            FnJoin('', ["arn:aws:iam::#{params[:accountids]['production']}:role/", Ref(:prefix), '-s3deploy'])
                        ]
                    }
                }
            ]
        )
    end

    # S3 Event rule - trigger CodePipeline when artifact added to S3 bucket
    Events_Rule(:s3eventrule) do
        EventPattern(
            'source': [
                'aws.s3'
            ],
            'detail-type': [
                'AWS API Call via CloudTrail'
            ],
            'detail': {
                eventSources: ['s3.amazonaws.com'],
                eventName: ['PutObject'],
                'resources': {
                    ARN: [
                        FnJoin('', [FnGetAtt(:artifactsbucket, 'Arn'), '/', Ref(:prefix), '.zip'])
                    ]
                }
            }
        )
        Name FnJoin('', [Ref(:prefix), '-bitbucket'])
        State 'ENABLED'
        Targets [
            {
                'Arn': FnJoin('', ['arn:aws:codepipeline:', Ref('AWS::Region'), ':', Ref('AWS::AccountId'), ':', Ref(:prefix)]),
                'Id': FnJoin('', ['s3Event-', Ref(:prefix)]),
                'RoleArn': FnGetAtt(:cloudwatcheventrole, 'Arn')
            }
        ]
    end

    #
    ##
    ### Lambda - slack notification function & lambda permission
    ##
    #

    # Lambda function - Slack Notify
    Serverless_Function(:slacknotify) do
        Handler 'resource.handler'
        Runtime 'nodejs10.x'
        CodeUri(
            Bucket: FnJoin('', [Ref(:prefix), '-pipeline-resources']),
            Key: Ref(:slacknotifykey)
        )
        Role FnGetAtt(:lambdaexecutionrole, 'Arn')
        Timeout 10
        Tags(params[:tags])
    end

    # Lambda Permission - Execute Slack Notify function from SNS topic
    Lambda_Permission(:slacknotifypermission) do
        Action 'lambda:InvokeFunction'
        FunctionName Ref(:slacknotify)
        Principal 'sns.amazonaws.com'
        SourceArn Ref(:manualapprovaltopic)
    end

    #
    ##
    ### Codebuild - projects for app & tests
    ##
    #

    # Codebuild project for core app stack
    CodeBuild_Project(:appcodebuildproject) do
        Name FnJoin('', [Ref(:prefix), '-appbuild'])
        Artifacts(
            Type: 'CODEPIPELINE'
        )
        Environment(
            ComputeType: 'BUILD_GENERAL1_SMALL',
            Image: FnJoin('', [Ref(:ecruri), ':latest']),
            Type: 'LINUX_CONTAINER',
            EnvironmentVariables: [
                {
                    Name: 'ARTIFACTS_BUCKET',
                    Value: Ref(:artifactsbucket)
                },
                {
                    Name: 'KMS_KEY_ID',
                    Value: Ref(:kmskey)
                },
                {
                    Name: 'PREFIX',
                    Value: Ref(:prefix)
                }
            ]
        )
        ServiceRole FnJoin('', ["arn:aws:iam::#{params[:accountids]['development']}:role/", Ref(:prefix), '-codebuild'])
        EncryptionKey FnGetAtt(:kmskey, 'Arn')
        Source(
            Type: 'CODEPIPELINE',
            BuildSpec: 'infra/pipeline/builds/app_buildspec.yml'
        )
        params[:tags].each_pair do |k, v|
            next if k == 'TIER'

            add_tag(k, v)
        end
        add_tag('TIER', 'Production')
    end

    # Codebuild project for app tests
    CodeBuild_Project(:codebuildapptests) do
        Name FnJoin('', [Ref(:prefix), '-apptests'])
        Description('Template validation & module tests')
        Artifacts(
            Type: 'CODEPIPELINE'
        )
        Environment(
            ComputeType: 'BUILD_GENERAL1_SMALL',
            Image: FnJoin('', [Ref(:ecruri), ':latest']),
            Type: 'LINUX_CONTAINER'
        )
        ServiceRole FnJoin('', ["arn:aws:iam::#{params[:accountids]['development']}:role/", Ref(:prefix), '-codebuild'])
        Source(
            Type: 'CODEPIPELINE',
            BuildSpec: 'infra/app/tests/appTests.yml'
        )
        params[:tags].each_pair do |k, v|
            next if k == 'TIER'

            add_tag(k, v)
        end
        add_tag('TIER', 'Production')
    end

    # Codebuild project for api integration tests
    CodeBuild_Project(:codebuildapitests) do
        Name FnJoin('', [Ref(:prefix), '-apitests'])
        Description('End-to-end tests on API Gateway')
        Artifacts(
            Type: 'CODEPIPELINE'
        )
        Environment(
            ComputeType: 'BUILD_GENERAL1_SMALL',
            Image: FnJoin('', [Ref(:ecruri), ':latest']),
            Type: 'LINUX_CONTAINER'
        )
        ServiceRole FnJoin('', ["arn:aws:iam::#{params[:accountids]['development']}:role/", Ref(:prefix), '-codebuild'])
        Source(
            Type: 'CODEPIPELINE',
            BuildSpec: 'infra/app/tests/apiTests.yml'
        )
    end

    #
    ##
    ### SNS Topics
    ##
    #

    # SNS topic for pipeline manual approvals, triggers Slack Notifications
    SNS_Topic(:manualapprovaltopic) do
        Subscription [
            {
                'Endpoint' => FnGetAtt(:slacknotify, 'Arn'),
                'Protocol' => 'lambda'
            }
        ]
    end

    #
    ##
    ### Code Pipeline - definition of pipeline for app deployments
    ##
    #
    CodePipeline_Pipeline(:codepipeline) do
        DependsOn :codepipelinepolicy
        Name Ref(:prefix)
        ArtifactStores([
            {
                ArtifactStore: {
                    Type: 'S3',
                    Location: Ref(:artifactsbucket),
                    EncryptionKey: {
                        Id: FnGetAtt(:kmskey, 'Arn'),
                        Type: 'KMS'
                    }
                },
                Region: Ref('AWS::Region')
            }
        ])
        RoleArn FnJoin('', ["arn:aws:iam::#{params[:accountids]['development']}:role/", Ref(:prefix), '-codepipeline'])
        Stages([
            {
                Name: 'RetrieveSource',
                Actions: [
                    {
                        Name: 'RetrieveAppSource',
                        ActionTypeId: {
                            Category: 'Source',
                            Owner: 'AWS',
                            Provider: 'S3',
                            Version: '1'
                        },
                        Configuration: {
                            S3Bucket: Ref(:artifactsbucket),
                            S3ObjectKey: FnJoin('', [Ref(:prefix), '.zip'])
                        },
                        OutputArtifacts: [{
                            Name: 'sourceout'
                        }]
                    }
                ]
            },
            {
                Name: 'AppBuild',
                Actions: [
                    {
                        Name: 'AppTests',
                        ActionTypeId: {
                            Category: 'Build',
                            Owner: 'AWS',
                            Provider: 'CodeBuild',
                            Version: '1'
                        },
                        Configuration: {
                            ProjectName: Ref(:codebuildapptests)
                        },
                        InputArtifacts: [{
                            Name: 'sourceout'
                        }],
                        RunOrder: 1
                    },
                    {
                        Name: 'AppBuild',
                        ActionTypeId: {
                            Category: 'Build',
                            Owner: 'AWS',
                            Provider: 'CodeBuild',
                            Version: '1'
                        },
                        Configuration: {
                            ProjectName: Ref(:appcodebuildproject)
                        },
                        InputArtifacts: [{
                            Name: 'sourceout'
                        }],
                        OutputArtifacts: [{
                            Name: 'appbuildout'
                        }],
                        RunOrder: 2
                    }
                ]
            },
            {
                Name: 'DEV_Deployment',
                Actions: [
                    {
                        Name: 'DEVAppChangeSet',
                        RoleArn: FnJoin('', ["arn:aws:iam::#{params[:accountids]['development']}:role/", Ref(:prefix), '-codepipelinecloudformationrole']),
                        ActionTypeId: {
                            Category: 'Deploy',
                            Owner: 'AWS',
                            Provider: 'CloudFormation',
                            Version: '1'
                        },
                        Configuration: {
                            ActionMode: 'CHANGE_SET_REPLACE',
                            Capabilities: 'CAPABILITY_NAMED_IAM,CAPABILITY_AUTO_EXPAND',
                            ChangeSetName: FnJoin('-', [Ref(:prefix), 'dev']),
                            OutputFileName: 'createstackoutput.json',
                            RoleArn: FnJoin('', ["arn:aws:iam::#{params[:accountids]['development']}:role/", Ref(:prefix), '-cloudformationdeployrole']),
                            StackName: FnJoin('-', [Ref(:prefix), 'dev']),
                            TemplatePath: FnJoin('', ['appbuildout::', Ref(:prefix), '.yaml']),
                            TemplateConfiguration: 'appbuildout::Development-config.json',
                            ParameterOverrides: FnJoin('', ['{ "prefix": "', Ref(:prefix), '", "deployenv": "dev"}'])
                        },
                        InputArtifacts: [{
                            Name: 'appbuildout'
                        }],
                        RunOrder: 1
                    },
                    {
                        Name: 'DeployDEVAppChangeSet',
                        RoleArn: FnJoin('', ["arn:aws:iam::#{params[:accountids]['development']}:role/", Ref(:prefix), '-codepipelinecloudformationrole']),
                        ActionTypeId: {
                            Category: 'Deploy',
                            Owner: 'AWS',
                            Provider: 'CloudFormation',
                            Version: '1'
                        },
                        Configuration: {
                            ActionMode: 'CHANGE_SET_EXECUTE',
                            Capabilities: 'CAPABILITY_NAMED_IAM,CAPABILITY_AUTO_EXPAND',
                            ChangeSetName: FnJoin('-', [Ref(:prefix), 'dev']),
                            OutputFileName: 'appstackoutput.json',
                            RoleArn: FnJoin('', ["arn:aws:iam::#{params[:accountids]['development']}:role/", Ref(:prefix), '-cloudformationdeployrole']),
                            StackName: FnJoin('-', [Ref(:prefix), 'dev']),
                            TemplatePath: FnJoin('', ['appbuildout::', Ref(:prefix), '.yaml']),
                            TemplateConfiguration: 'appbuildout::Development-config.json'
                        },
                        InputArtifacts: [{
                            Name: 'appbuildout'
                        }],
                        RunOrder: 2
                    }
                ]
            },
            {
                Name: 'APIIntegrationTests',
                Actions: [{
                    Name: 'APIIntegrationTests',
                    ActionTypeId: {
                        Category: 'Test',
                        Owner: 'AWS',
                        Provider: 'CodeBuild',
                        Version: '1'
                    },
                    Configuration: {
                        ProjectName: Ref(:codebuildapitests)
                    },
                    InputArtifacts: [{
                        Name: 'sourceout'
                    }],
                    RunOrder: 1
                }]
            },
            {
                Name: 'UAT_Deployment',
                Actions: [
                    {
                        Name: 'UATAppChangeSet',
                        RoleArn: FnJoin('', ["arn:aws:iam::#{params[:accountids]['testing']}:role/", Ref(:prefix), '-codepipelinecloudformationrole']),
                        ActionTypeId: {
                            Category: 'Deploy',
                            Owner: 'AWS',
                            Provider: 'CloudFormation',
                            Version: '1'
                        },
                        Configuration: {
                            ActionMode: 'CHANGE_SET_REPLACE',
                            Capabilities: 'CAPABILITY_NAMED_IAM,CAPABILITY_AUTO_EXPAND',
                            ChangeSetName: FnJoin('-', [Ref(:prefix), 'test']),
                            OutputFileName: 'createstackoutput.json',
                            RoleArn: FnJoin('', ["arn:aws:iam::#{params[:accountids]['testing']}:role/", Ref(:prefix), '-cloudformationdeployrole']),
                            StackName: FnJoin('-', [Ref(:prefix), 'test']),
                            TemplatePath: FnJoin('', ['appbuildout::', Ref(:prefix), '.yaml']),
                            TemplateConfiguration: 'appbuildout::Testing-config.json',
                            ParameterOverrides: FnJoin('', ['{ "prefix": "', Ref(:prefix), '", "deployenv": "test"}'])
                        },
                        InputArtifacts: [{
                            Name: 'appbuildout'
                        }],
                        RunOrder: 1
                    },
                    {
                        Name: 'UATApproval',
                        ActionTypeId: {
                            Category: 'Approval',
                            Owner: 'AWS',
                            Provider: 'Manual',
                            Version: '1'
                        },
                        Configuration: {
                            'NotificationArn': Ref(:manualapprovaltopic)
                        },
                        RunOrder: 2
                    },
                    {
                        Name: 'DeployUATAppChangeSet',
                        RoleArn: FnJoin('', ["arn:aws:iam::#{params[:accountids]['testing']}:role/", Ref(:prefix), '-codepipelinecloudformationrole']),
                        ActionTypeId: {
                            Category: 'Deploy',
                            Owner: 'AWS',
                            Provider: 'CloudFormation',
                            Version: '1'
                        },
                        Configuration: {
                            ActionMode: 'CHANGE_SET_EXECUTE',
                            Capabilities: 'CAPABILITY_NAMED_IAM,CAPABILITY_AUTO_EXPAND',
                            ChangeSetName: FnJoin('-', [Ref(:prefix), 'test']),
                            OutputFileName: 'appstackoutput.json',
                            RoleArn: FnJoin('', ["arn:aws:iam::#{params[:accountids]['testing']}:role/", Ref(:prefix), '-cloudformationdeployrole']),
                            StackName: FnJoin('-', [Ref(:prefix), 'test']),
                            TemplatePath: FnJoin('', ['appbuildout::', Ref(:prefix), '.yaml']),
                            TemplateConfiguration: 'appbuildout::Testing-config.json'
                        },
                        InputArtifacts: [{
                            Name: 'appbuildout'
                        }],
                        RunOrder: 3
                    }
                ]
            },
            {
                Name: 'PROD_Deployment',
                Actions: [
                    {
                        Name: 'PRODAppChangeSet',
                        RoleArn: FnJoin('', ["arn:aws:iam::#{params[:accountids]['production']}:role/", Ref(:prefix), '-codepipelinecloudformationrole']),
                        ActionTypeId: {
                            Category: 'Deploy',
                            Owner: 'AWS',
                            Provider: 'CloudFormation',
                            Version: '1'
                        },
                        Configuration: {
                            ActionMode: 'CHANGE_SET_REPLACE',
                            Capabilities: 'CAPABILITY_NAMED_IAM,CAPABILITY_AUTO_EXPAND',
                            ChangeSetName: FnJoin('-', [Ref(:prefix), 'prod']),
                            OutputFileName: 'createstackoutput.json',
                            RoleArn: FnJoin('', ["arn:aws:iam::#{params[:accountids]['production']}:role/", Ref(:prefix), '-cloudformationdeployrole']),
                            StackName: FnJoin('-', [Ref(:prefix), 'prod']),
                            TemplatePath: FnJoin('', ['appbuildout::', Ref(:prefix), '.yaml']),
                            TemplateConfiguration: 'appbuildout::Production-config.json',
                            ParameterOverrides: FnJoin('', ['{ "prefix": "', Ref(:prefix), '", "deployenv": "prod"}'])
                        },
                        InputArtifacts: [{
                            Name: 'appbuildout'
                        }],
                        RunOrder: 1
                    },
                    {
                        Name: 'PRODApproval',
                        ActionTypeId: {
                            Category: 'Approval',
                            Owner: 'AWS',
                            Provider: 'Manual',
                            Version: '1'
                        },
                        Configuration: {
                            'NotificationArn': Ref(:manualapprovaltopic)
                        },
                        RunOrder: 2
                    },
                    {
                        Name: 'DeployPRODAppChangeSet',
                        RoleArn: FnJoin('', ["arn:aws:iam::#{params[:accountids]['production']}:role/", Ref(:prefix), '-codepipelinecloudformationrole']),
                        ActionTypeId: {
                            Category: 'Deploy',
                            Owner: 'AWS',
                            Provider: 'CloudFormation',
                            Version: '1'
                        },
                        Configuration: {
                            ActionMode: 'CHANGE_SET_EXECUTE',
                            Capabilities: 'CAPABILITY_NAMED_IAM,CAPABILITY_AUTO_EXPAND',
                            ChangeSetName: FnJoin('-', [Ref(:prefix), 'prod']),
                            OutputFileName: 'appstackoutput.json',
                            RoleArn: FnJoin('', ["arn:aws:iam::#{params[:accountids]['production']}:role/", Ref(:prefix), '-cloudformationdeployrole']),
                            StackName: FnJoin('-', [Ref(:prefix), 'prod']),
                            TemplatePath: FnJoin('', ['appbuildout::', Ref(:prefix), '.yaml']),
                            TemplateConfiguration: 'appbuildout::Production-config.json'
                        },
                        InputArtifacts: [{
                            Name: 'appbuildout'
                        }],
                        RunOrder: 3
                    }
                ]
            }
        ])
    end
end
