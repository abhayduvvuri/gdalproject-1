#!/usr/bin/env bash

set -o errexit

for path in $( find "${CODEBUILD_SRC_DIR}/src" -name index.js | grep -v node_modules | sed "s[/index.js[[" )
do
    cd "${path}" || exit 1
    npm install
    eslint ./*.js ./test/*.js
    npm test
done
