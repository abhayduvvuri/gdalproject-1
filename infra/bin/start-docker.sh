#!/usr/bin/env bash
ECRURI=$(grep ecruri: infra/config/app.yml | sed "s/ecruri: //g"  | tr -d ' ')
AWSPROFILE=$(grep awsprofile_dev: infra/config/app.yml | sed "s/awsprofile_dev: //g"  | tr -d ' ')
export AWS_PROFILE=$AWSPROFILE
$(aws ecr get-login --no-include-email)
docker pull $ECRURI:latest
docker run --rm -it -e AWS_SDK_LOAD_CONFIG=1 -e AWS_PROFILE=$AWSPROFILE -e CODEBUILD_SRC_DIR=/app -v ~/.aws:/root/.aws -v "$(pwd):/app" $ECRURI:latest /bin/bash
