# frozen_string_literal: true

require 'cfndsl'

CloudFormation do
    params = {}
    external_parameters.each_pair do |key, val|
        key = key.to_sym
        params[key] = val
    end
    Description 'GDAL Project App Stack'
    Transform 'AWS::Serverless-2016-10-31'

    #
    ##
    ### Parameters
    ##
    #

    Parameter(:prefix) do
        Type String
        AllowedPattern '[a-z]*[-a-z0-9]*'
        ConstraintDescription 'A lower case string between 3 and 16 characters'
        Description 'A lower case string between 3 and 16 characters'
        MinLength 3
        MaxLength 16
    end

    Parameter(:deployenv) do
        Type String
        AllowedPattern '[a-z]*[-a-z0-9]*'
        ConstraintDescription 'A lower case string between 3 and 16 characters'
        Description 'A lower case string between 3 and 16 characters'
        MinLength 3
        MaxLength 16
    end

    Parameter(:reprojectimage) do
        Type String
        AllowedPattern '[a-z]*[-a-z0-9]*'
        ConstraintDescription 'A lower case string between 3 and 16 characters'
        Description 'A lower case string between 3 and 16 characters'
        MinLength 3
        MaxLength 16
    end

    Parameter('Certificate') do
        Type('String')
        Default('arn:aws:acm:region:123456789012:certificate/00000000-0000-0000-0000-000000000000')
    end

    Parameter('ServiceName') do
        Type('String')
        Default('GDALService')
    end

    Parameter('ContainerPort') do
        Type('Number')
        Default(80)
    end

    Parameter('LoadBalancerPort') do
        Type('Number')
        Default(80)
    end

    Parameter('HealthCheckPath') do
        Type('String')
        Default('/')
    end

    Parameter('HostedZoneName') do
        Type('String')
        Default('customerservice.nsw.gov.au')
    end

    Parameter('Subdomain') do
        Type('String')
        Default('myservice')
    end

    Parameter('MinContainers') do
        Type('Number')
        Default(0)
    end

    Parameter('MaxContainers') do
        Type('Number')
        Default(10)
    end

    Parameter('DesiredCount') do
        Type('Number')
        Default(0)
    end

    Parameter('AutoScalingTargetValue') do
        Type('Number')
        Default(50)
    end

    S3_Bucket(:gdalBucket) do
        BucketName FnJoin('-', [Ref(:prefix), Ref(:deployenv), 'gdal-cache-content'])
        DependsOn([:ProcessingLambdaPermission])
        DeletionPolicy 'Retain'
        Property('NotificationConfiguration', {
                     'LambdaConfigurations': [{
                         'Event': 's3:ObjectCreated:*',
                         'Filter': {
                             'S3Key': {
                                 'Rules': [{
                                     'Name': 'suffix',
                                     'Value': '.timestamp'
                                 }]
                             }
                         },
                         'Function': FnJoin('', [FnGetAtt(:ProcessingLambdaFunction, 'Arn')])
                     }]
                 })
    end

    Resource(:BaseExecutionRole) do
        Type 'AWS::IAM::Role'
        Property('RoleName', FnJoin('-', [Ref(:prefix), Ref(:deployenv), 'GDALService-ProcessingLambda_ExecRole']))
        Property('AssumeRolePolicyDocument', {
                     'Version': '2012-10-17',
                     'Statement': [
                         {
                             'Action': ['sts:AssumeRole'],
                             'Effect': 'Allow',
                             'Principal': {
                                 'Service':
                                 [
                                     'lambda.amazonaws.com',
                                     'ecs-tasks.amazonaws.com',
                                     'ecs.amazonaws.com',
                                     's3.amazonaws.com'
                                 ]
                             }
                         }
                     ]
                 })
        Property('Policies', [
            {
                'PolicyName': FnJoin('-', [Ref(:prefix), Ref(:deployenv), 'lambda-pass-role']),
                'PolicyDocument': {
                    'Version': '2012-10-17',
                    'Statement': [
                        {
                            'Effect': 'Allow',
                            'Action': ['iam:PassRole'],
                            'Resource': '*'
                        }
                    ]
                }
            },
            {
                'PolicyName': FnJoin('-', [Ref(:prefix), Ref(:deployenv), 'allowLogging']),
                'PolicyDocument': {
                    'Version': '2012-10-17',
                    'Statement': [
                        {
                            'Effect': 'Allow',
                            'Action': ['logs:*'],
                            'Resource': 'arn:aws:logs:*:*:*'
                        }
                    ]
                }
            },
            {
                'PolicyName': FnJoin('-', [Ref(:prefix), Ref(:deployenv), 'allowS3']),
                'PolicyDocument': {
                    'Version': '2012-10-17',
                    'Statement': [
                        {
                            'Effect': 'Allow',
                            'Action': ['s3:GetObject', 's3:DeleteObject'],
                            'Resource': FnJoin('', ['arn:aws:s3:::', FnJoin('-', [Ref(:prefix), Ref(:deployenv), 'gdal-cache-content']), '/*'])
                        }
                    ]
                }
            },
            {
                'PolicyDocument' => {
                    'Statement' => [
                        {
                            'Action' => [
                                'ecs:RunTask',
                                'ecs:StartTask',
                                'ecs:StopTask',
                                'ecs:ListTasks'
                            ],
                            'Effect' => 'Allow',
                            'Resource' => 'arn:aws:ecs:*:*:*'
                        }
                    ],
                    'Version' => '2012-10-17'
                },
                'PolicyName' => 'allowEcsTask'
            }
        ])
    end

    Serverless_Function(:ProcessingLambdaFunction) do
        FunctionName FnJoin('-', [Ref(:prefix), Ref(:deployenv), 'GDALService-ProcessingLambda'])
        Handler 'index.handler'
        Runtime 'nodejs10.x'
        CodeUri '../../src/js/trigger'
        Role FnGetAtt(:BaseExecutionRole, 'Arn')
        Timeout 3
        ReservedConcurrentExecutions 5
        Tags(params[:tags])
        Property('Environment', {
                     'Variables' => {
                         'accId' => Ref('AWS::AccountId'),
                         'svcName' => Ref('ServiceName'),
                         'prefix' => Ref(:prefix),
                         'subnetA' => Ref('SubnetA'),
                         'subnetB' => Ref('SubnetB'),
                         'deployenv' => Ref(:deployenv)
                     }
                 })
    end

    Lambda_Permission(:ProcessingLambdaPermission) do
        DependsOn :ProcessingLambdaFunction
        Action 'lambda:InvokeFunction'
        FunctionName FnJoin('', [Ref(:ProcessingLambdaFunction)])
        Principal 's3.amazonaws.com'
        SourceArn FnJoin('', ['arn:aws:s3:::', FnJoin('-', [Ref(:prefix), Ref(:deployenv), 'gdal-cache-content'])])
        SourceAccount Ref('AWS::AccountId')
    end

    VPC(:VPC) do
        EnableDnsSupport true
        EnableDnsHostnames true
        CidrBlock '10.20.0.0/16'
        add_tag('Name', FnJoin('-', [Ref(:prefix), Ref(:deployenv), 'VPC']))
    end

    InternetGateway(:InternetGateway) do
        add_tag('Name', FnJoin('-', [Ref(:prefix), Ref(:deployenv), 'Gateway']))
    end

    VPCGatewayAttachment(:GatewayToInternet) do
        DependsOn :VPC
        VpcId Ref(:VPC)
        InternetGatewayId Ref(:InternetGateway)
    end

    Subnet(:SubnetA) do
        DependsOn :VPC
        VpcId Ref(:VPC)
        CidrBlock '10.20.10.0/24'
        AvailabilityZone 'ap-southeast-2a'
        add_tag('Name', 'SubnetA')
    end

    RouteTable(:routeTableA) do
        DependsOn :VPC
        VpcId Ref(:VPC)
        add_tag('Name', 'SubnetARouteTable')
    end

    SubnetRouteTableAssociation(:RoutetableAsscA) do
        SubnetId Ref(:SubnetA)
        RouteTableId Ref(:routeTableA)
    end

    EC2_Route(:SubnetGatewayRouteA) do
        DependsOn :GatewayToInternet
        RouteTableId Ref(:routeTableA)
        DestinationCidrBlock '0.0.0.0/0'
        GatewayId Ref(:InternetGateway)
    end

    Subnet(:SubnetB) do
        DependsOn :VPC
        VpcId Ref(:VPC)
        CidrBlock '10.20.11.0/24'
        AvailabilityZone 'ap-southeast-2b'
        add_tag('Name', 'SubnetB')
    end

    RouteTable(:routeTableB) do
        DependsOn :VPC
        VpcId Ref(:VPC)
        add_tag('Name', 'SubnetBRouteTable')
    end

    SubnetRouteTableAssociation(:RoutetableAsscB) do
        SubnetId Ref(:SubnetB)
        RouteTableId Ref(:routeTableB)
    end

    EC2_Route(:SubnetGatewayRouteB) do
        DependsOn :GatewayToInternet
        RouteTableId Ref(:routeTableB)
        DestinationCidrBlock '0.0.0.0/0'
        GatewayId Ref(:InternetGateway)
    end

    Resource('Cluster') do
        Type('AWS::ECS::Cluster')
        Property('ClusterName', FnJoin('-', [
            Ref('prefix'),
            Ref('deployenv'),
            Ref('ServiceName'),
            'Cluster'
        ]))
    end

    Resource('TaskDefinition') do
        Type('AWS::ECS::TaskDefinition')
        DependsOn('LogGroup')
        Property('Family', FnJoin('-', [
            Ref('prefix'),
            Ref('deployenv'),
            Ref('ServiceName'),
            'TaskDefinition'
        ]))
        Property('NetworkMode', 'awsvpc')
        Property('RequiresCompatibilities', [
            'FARGATE'
        ])
        Property('Cpu', 4096)
        Property('Memory', '8GB')
        Property('EphemeralStorage', { "sizeInGiB": 100 })
        Property('ExecutionRoleArn', Ref('ExecutionRole'))
        Property('TaskRoleArn', Ref('TaskRole'))
        Property('ContainerDefinitions', [
            {
                'Image' => FnJoin('', [Ref('AWS::AccountId'), '.dkr.ecr.ap-southeast-2.amazonaws.com/', Ref(deployenv), '-', Ref(reprojectimage), ':latest']),
                'LogConfiguration' => {
                    'LogDriver' => 'awslogs',
                    'Options' => {
                        'awslogs-group' => Ref('LogGroup'),
                        'awslogs-region' => Ref('AWS::Region'),
                        'awslogs-stream-prefix' => 'ecs'
                    }
                },
                'Name' => Ref('ServiceName'),
                'PortMappings' => [
                    {
                        'ContainerPort' => Ref('ContainerPort')
                    }
                ]
            }
        ])
    end

    Resource('ExecutionRole') do
        Type('AWS::IAM::Role')
        Property('RoleName', FnJoin('-', [
            Ref('prefix'),
            Ref('deployenv'),
            Ref('ServiceName'),
            'ExecutionRole'
        ]))
        Property('AssumeRolePolicyDocument', {
                     'Statement' => [
                         {
                             'Action' => 'sts:AssumeRole',
                             'Effect' => 'Allow',
                             'Principal' => {
                                 'Service' => 'ecs-tasks.amazonaws.com'
                             }
                         }
                     ]
                 })
        Property('ManagedPolicyArns', [
            'arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy'
        ])
    end

    Resource('TaskRole') do
        Type('AWS::IAM::Role')
        Property('RoleName', FnJoin('-', [
            Ref('prefix'),
            Ref('deployenv'),
            Ref('ServiceName'),
            'TaskRole'
        ]))
        Property('AssumeRolePolicyDocument', {
                     'Statement' => [
                         {
                             'Action' => 'sts:AssumeRole',
                             'Effect' => 'Allow',
                             'Principal' => {
                                 'Service' => 'ecs-tasks.amazonaws.com'
                             }
                         }
                     ]
                 })
        Property('Policies', [
            {
                'PolicyDocument' => {
                    'Statement' => [
                        {
                            'Action' => [
                                'logs:*'
                            ],
                            'Effect' => 'Allow',
                            'Resource' => 'arn:aws:logs:*:*:*'
                        }
                    ],
                    'Version' => '2012-10-17'
                },
                'PolicyName' => 'allowLogging'
            },
            {
                'PolicyDocument' => {
                    'Statement' => [
                        {
                            'Action' => [
                                's3:*'
                            ],
                            'Effect' => 'Allow',
                            'Resource' => '*'
                        }
                    ],
                    'Version' => '2012-10-17'
                },
                'PolicyName' => 'getAndDeleteObjects'
            },
            {
                'PolicyDocument' => {
                    'Statement' => [
                        {
                            'Action' => [
                                'ecs:*'
                            ],
                            'Effect' => 'Allow',
                            'Resource' => '*'
                        }
                    ],
                    'Version' => '2012-10-17'
                },
                'PolicyName' => 'allowEcsAccess'
            }
        ])
    end

    Resource('AutoScalingRole') do
        Type('AWS::IAM::Role')
        Property('RoleName', FnJoin('-', [
            Ref('prefix'),
            Ref('deployenv'),
            Ref('ServiceName'),
            'AutoScalingRole'
        ]))
        Property('AssumeRolePolicyDocument', {
                     'Statement' => [
                         {
                             'Action' => 'sts:AssumeRole',
                             'Effect' => 'Allow',
                             'Principal' => {
                                 'Service' => 'ecs-tasks.amazonaws.com'
                             }
                         }
                     ]
                 })
        Property('ManagedPolicyArns', [
            'arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceAutoscaleRole'
        ])
    end

    Resource('ContainerSecurityGroup') do
        Type('AWS::EC2::SecurityGroup')
        Property('GroupDescription', FnJoin('-', [
            Ref('prefix'),
            Ref('deployenv'),
            Ref('ServiceName'),
            'ContainerSecurityGroup'
        ]))
        Property('VpcId', Ref('VPC'))
        Property('SecurityGroupIngress', [
            {
                'FromPort' => Ref('ContainerPort'),
                'IpProtocol' => 'tcp',
                'SourceSecurityGroupId' => Ref('LoadBalancerSecurityGroup'),
                'ToPort' => Ref('ContainerPort')
            }
        ])
    end

    Resource('LoadBalancerSecurityGroup') do
        Type('AWS::EC2::SecurityGroup')
        Property('GroupDescription', FnJoin('-', [
            Ref('prefix'),
            Ref('deployenv'),
            Ref('ServiceName'),
            'LoadBalancerSecurityGroup'
        ]))
        Property('VpcId', Ref('VPC'))
        Property('SecurityGroupIngress', [
            {
                'CidrIp' => '0.0.0.0/0',
                'FromPort' => Ref('LoadBalancerPort'),
                'IpProtocol' => 'tcp',
                'ToPort' => Ref('LoadBalancerPort')
            }
        ])
    end

    Resource('Service') do
        Type('AWS::ECS::Service')
        DependsOn([
            'ListenerHTTP'
        ])
        Property('ServiceName', FnJoin('-', [
            Ref('prefix'),
            Ref('deployenv'),
            Ref('ServiceName')
        ]))
        Property('Cluster', Ref('Cluster'))
        Property('TaskDefinition', Ref('TaskDefinition'))
        Property('DeploymentConfiguration', {
                     'MaximumPercent' => 200,
                     'MinimumHealthyPercent' => 100
                 })
        Property('DesiredCount', Ref('DesiredCount'))
        Property('HealthCheckGracePeriodSeconds', 30)
        Property('LaunchType', 'FARGATE')
        Property('NetworkConfiguration', {
                     'AwsvpcConfiguration' => {
                         'AssignPublicIp' => 'ENABLED',
                         'SecurityGroups' => [
                             Ref('ContainerSecurityGroup')
                         ],
                         'Subnets' => [
                             Ref('SubnetA'),
                             Ref('SubnetB')
                         ]
                     }
                 })
        Property('LoadBalancers', [
            {
                'ContainerName' => Ref('ServiceName'),
                'ContainerPort' => Ref('ContainerPort'),
                'TargetGroupArn' => Ref('TargetGroup')
            }
        ])
    end

    Resource('TargetGroup') do
        Type('AWS::ElasticLoadBalancingV2::TargetGroup')
        Property('HealthCheckIntervalSeconds', 10)
        Property('HealthCheckPath', Ref('HealthCheckPath'))
        Property('HealthCheckTimeoutSeconds', 5)
        Property('UnhealthyThresholdCount', 2)
        Property('HealthyThresholdCount', 2)
        Property('Name', FnJoin('-', [
            Ref('prefix'),
            Ref('deployenv'),
            'TargetGroup'
        ]))
        Property('Port', Ref('ContainerPort'))
        Property('Protocol', 'HTTP')
        Property('TargetGroupAttributes', [
            {
                'Key' => 'deregistration_delay.timeout_seconds',
                'Value' => 60
            }
        ])
        Property('TargetType', 'ip')
        Property('VpcId', Ref('VPC'))
    end

    Resource('ListenerHTTP') do
        Type('AWS::ElasticLoadBalancingV2::Listener')
        Property('DefaultActions', [
            {
                'TargetGroupArn' => Ref('TargetGroup'),
                'Type' => 'forward'
            }
        ])
        Property('LoadBalancerArn', Ref('LoadBalancer'))
        Property('Port', Ref('LoadBalancerPort'))
        Property('Protocol', 'HTTP')
    end

    Resource('LoadBalancer') do
        Type('AWS::ElasticLoadBalancingV2::LoadBalancer')
        Property('LoadBalancerAttributes', [
            {
                'Key' => 'idle_timeout.timeout_seconds',
                'Value' => 60
            }
        ])
        Property('Name', FnJoin('-', [
            Ref('prefix'),
            Ref('deployenv'),
            'lb'
        ]))
        Property('Scheme', 'internet-facing')
        Property('SecurityGroups', [
            Ref('LoadBalancerSecurityGroup')
        ])
        Property('Subnets', [
            Ref('SubnetA'),
            Ref('SubnetB')
        ])
    end

    Resource('LogGroup') do
        Type('AWS::Logs::LogGroup')
        Property('LogGroupName', FnJoin('-', [
            '/ecs/',
            Ref('prefix'),
            Ref('deployenv'),
            Ref('ServiceName'),
            'TaskDefinition'
        ]))
    end

    Resource('AutoScalingTarget') do
        Type('AWS::ApplicationAutoScaling::ScalableTarget')
        Property('MinCapacity', Ref('MinContainers'))
        Property('MaxCapacity', Ref('MaxContainers'))
        Property('ResourceId', FnJoin('/', [
            'service',
            Ref('Cluster'),
            FnGetAtt('Service', 'Name')
        ]))
        Property('ScalableDimension', 'ecs:service:DesiredCount')
        Property('ServiceNamespace', 'ecs')
        Property('RoleARN', FnGetAtt('AutoScalingRole', 'Arn'))
    end

    Resource('AutoScalingPolicy') do
        Type('AWS::ApplicationAutoScaling::ScalingPolicy')
        Property('PolicyName', FnJoin('-', [
            Ref('prefix'),
            Ref('deployenv'),
            Ref('ServiceName'),
            'AutoScalingPolicy'
        ]))
        Property('PolicyType', 'TargetTrackingScaling')
        Property('ScalingTargetId', Ref('AutoScalingTarget'))
        Property('TargetTrackingScalingPolicyConfiguration', {
                     'PredefinedMetricSpecification' => {
                         'PredefinedMetricType' => 'ECSServiceAverageCPUUtilization'
                     },
                     'ScaleInCooldown' => 10,
                     'ScaleOutCooldown' => 10,
                     'TargetValue' => Ref('AutoScalingTargetValue')
                 })
    end
end
